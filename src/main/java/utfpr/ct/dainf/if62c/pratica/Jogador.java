package utfpr.ct.dainf.if62c.pratica;
public class Jogador {
    private int num;
    private String name;

    public Jogador(int num, String name) {
        this.num = num;
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public void setNum(int numero) {
        this.num = numero;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return num + " - " + name;
    }
}
